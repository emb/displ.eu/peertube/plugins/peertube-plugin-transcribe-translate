async function register({
  registerHook,
  registerSetting,
  settingsManager,
  peertubeHelpers,
}) {
  registerSetting({
    name: "trigger-transcription-and-translation",
    label: "URL to trigger the transcription and translation",
    type: "input",
    private: true,
  });

  registerHook({
    target: "action:api.video.uploaded",
    handler: async ({ video }) => {
      if (!video) {
        peertubeHelpers.logger.info(
          `Video is missing in action:api.video.uploaded`
        );
        return;
      }
      peertubeHelpers.logger.info(
        `Transposer only triggered when update the video`
      );
    },
  });

  registerHook({
    target: "action:api.video.updated",
    handler: async ({ video, body }) => {
      if (!video) {
        peertubeHelpers.logger.info(
          `Video is missing in action:api.video.updated`
        );
        return;
      }
      if (!body.pluginData) {
        peertubeHelpers.logger.info(
          `pluginData is missing in action:api.video.updated`
        );
        return;
      }
      if (!body.pluginData["trigger-transposer"]) {
        peertubeHelpers.logger.info(`"trigger transposer is false`);
        return;
      }
      body.pluginData["trigger-transposer"] = false;
      triggerTransposer(video);
    },
  });

  async function triggerTransposer(video) {
    const settings = await settingsManager.getSettings([
      "trigger-transcription-and-translation",
    ]);
    if (!settings["trigger-transcription-and-translation"]) {
      peertubeHelpers.logger.info(
        "trigger-transcription-and-translation setting is missing"
      );
      return;
    }

    const webserverUrl = peertubeHelpers.config.getWebserverUrl();
    if (!webserverUrl) {
      peertubeHelpers.logger.info(
        `webserverUrl is missing in action:api.video.uploaded`
      );
      return;
    }

    if (!video) {
      peertubeHelpers.logger.info(`Video is missing in triggerTransposer`);
      return;
    }
    if (!webserverUrl) {
      peertubeHelpers.logger.info(
        `webserverUrl is missing in triggerTransposer`
      );
      return;
    }

    const { language, id } = video;
    peertubeHelpers.logger.info(
      `Transposerconnect with ${id}, and language ${language}`
    );

    if (!language) {
      peertubeHelpers.logger.info(
        `Video "${id}" doesn't have a language defined.`
      );
    }

    const videoFiles = await getVideoFiles(id, peertubeHelpers);
    if (videoFiles.length === 0) {
      peertubeHelpers.logger.info(
        `No video file found for video "${id}", skipping.`
      );
      return;
    }
    peertubeHelpers.logger.info(
      `Sending transcription for this instance "${webserverUrl}" to Peertube Adpater`
    );
    peertubeHelpers.logger.info(
      `Sending transcription request with ID "${id}" in "${language}" to Peertube Adpater`
    );

    if (videoFiles && videoFiles.length > 0 && videoFiles[0].url) {
      var url = videoFiles[0].url; // TODO get the lowest video quality
      peertubeHelpers.logger.info(`${url} this url is saved in videoFiles[0]`);

      //not transcoded
      if (url.includes("web-videos")) {
        peertubeHelpers.logger.info(
          "start trigger transcription and translation with web-videos url"
        );
        triggerTranscriptionAndTranslation(
          video.id,
          url,
          language,
          settings["trigger-transcription-and-translation"],
          webserverUrl
        );
      } else if (url.includes("streaming-playlist")) {
        //transcoded
        peertubeHelpers.logger.info(
          "start trigger transcription and translation with streaming-playlist url"
        );
        triggerTranscriptionAndTranslation(
          video.id,
          url,
          language,
          settings["trigger-transcription-and-translation"],
          webserverUrl
        );
      } else {
        peertubeHelpers.logger.info(
          "start trigger transcription and translation without url include"
        );
        peertubeHelpers.logger.info("with url:", url);
        triggerTranscriptionAndTranslation(
          video.id,
          url,
          language,
          settings["trigger-transcription-and-translation"],
          webserverUrl
        );
      }
    } else {
      peertubeHelpers.logger.info("videoFiles does not have a valid URL.");
    }
  }

  /*
  //TODO Make somthing when caption is deleted?
  registerHook({
    target: 'action:api.video-caption.deleted',
    handler: videoController.captionDeleted.bind(videoController),
  });
  */

  //TODO: Error videoFiles: "uncaughtException: Cannot read properties of undefined (reading 'videoFiles')"
  async function getVideoFiles(id, peertubeHelpers) {
    peertubeHelpers.logger.info(`Fetching video files for video ID: ${id}`);

    const videoFilesData = await peertubeHelpers.videos.getFiles(id);
    if (!videoFilesData) {
      peertubeHelpers.logger.info(
        `No video files data returned for video ID: ${id}`
      );
      return [];
    }
    const webVideoFiles = videoFilesData.webVideo?.videoFiles || [];
    const hlsVideoFiles = videoFilesData.hls?.videoFiles || [];

    if (webVideoFiles.length === 0 && hlsVideoFiles.length === 0) {
      peertubeHelpers.logger.info(`No video files found for video ID: ${id}`);
      return [];
    }

    const combinedVideoFiles = webVideoFiles.concat(hlsVideoFiles);

    // Sortiere die Dateien nach Größe (aufsteigend)
    const sortedVideoFiles = combinedVideoFiles.sort(
      ({ size: sizeA }, { size: sizeB }) => sizeA - sizeB
    );

    peertubeHelpers.logger.info(
      `Sorted video files for video ID ${id}:`,
      sortedVideoFiles
    );

    return sortedVideoFiles;
  }

  function triggerTranscriptionAndTranslation(
    videoId,
    downloadUrl,
    language,
    triggerUrl,
    webserverUrl
  ) {
    if (!triggerUrl) {
      return;
    }
    peertubeHelpers.logger.info("triggerUrl:", triggerUrl);

    let body = {
      videoId: videoId,
      url: downloadUrl,
      peertubeInstanceBaseDomain: webserverUrl,
    };

    if (language) {
      body.language = language;
    }

    fetch(`${triggerUrl}/transcribe-and-translate/video`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    })
      .then((response) => {
        if (!response.ok) {
          response.json().then((json_error) => {
            throw new Error(
              `HTTP error! status: ${response.status} and message ${json_error.message}`
            );
          });
        }

        return response.json();
      })
      .then((data) => {
        peertubeHelpers.logger.info(
          `json result from server ${
            data.success ? "successful" : "unsuccessful"
          }`
        );
      })
      .catch((error) => {
        peertubeHelpers.logger.info(
          `Failed to report transcription done (for translation) "${error}", skipping.`
        );
      });
  }
}

async function unregister() {
  return;
}

module.exports = {
  register,
  unregister,
};
