function register({ registerHook, peertubeHelpers }) {
  registerHook({
    target: "action:admin-plugin-settings.init",
    handler: ({ npmName }) => {
      if ("peertube-plugin-transcription" !== npmName) {
        return;
      }

      // hide language model settings without any options
      // @todo should use LANGUAGE_STORAGE_KEY_PREFIX
      document
        .querySelectorAll(`.form-group[id^='language_']`)
        .forEach((element) => {
          const options = element.querySelectorAll("select option");
          if (options.length === 0) {
            element.classList.add("form-group--hidden");
          } else {
            element.classList.remove("form-group-hidden");
          }
        });
    },
  });

  registerHook({
    target: "action:video-watch.video.loaded",
    handler: ({ video }) => {
      fetch(
        `${peertubeHelpers.getBaseRouterRoute()}/videos/${video.uuid}/captions`,
        {
          method: "PUT",
          headers: peertubeHelpers.getAuthHeader(),
        }
      )
        .then((res) => res.json())
        .then((data) => console.log("Hi %s.", data));
    },
  });
}

export { register };
