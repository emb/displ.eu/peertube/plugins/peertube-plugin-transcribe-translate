async function register({ registerVideoField, peertubeHelpers }) {
  for (const type of ["import-url", "import-torrent", "update"]) {
    const videoFormOptions = {
      tab: "main",
    };

    registerVideoField(
      {
        name: "trigger-transposer",
        label: "Create subtitles in 20 languages",
        type: "input-checkbox",
        hidden: false,
        error: false,
      },
      {
        type,
        ...videoFormOptions,
        value: false,
      }
    );
  }
}

export { register };
