# Transposer connector

A PeerTube language tool plugin.

Standard scenario is to transcribe and translate with Whisper (by OpenAI). 

Developed by fairkom.

## Features

- Automated transcription of any video after uploading     
- Translation of transcript into several languages 

## Install

- npm run build 
- peertube-cli plugins install <path to plugin>

! INFO: Please mind that the name of the root repo folder should be the same name like the plugin. Change to `peertube-plugin-transcribe-translate` to `peertube-plugin-transposer-connector` with `mv peertube-plugin-transcribe-translate peertube-plugin-transposer-connector`. Name of repo got changed.  

Or use the built in plugin install mechanism as a PeerTube admin.



## Dependency

You need a Language Transposer Service account.  

Send an email for test access to Language Transposer Service to support@fairkom.eu and add the URL in the configuration of the admin interface of the plugin. 

## How it works

For high volume PeerTube sites a parallel and asynchronous handling of transcription and translation requests was our goal for the architecture. We developed a Transposer engine, that offers a queuing mechanism and endpoints for transcription and translations. Transposer offers several adapters, one of them is for an engine that runs Whisper.  Transposer uses a Kafka pipeline and Kong for creating adapters and APIs.

## Status

This plugin is now available for beta testing. 

Transposer is still work in progress - codebase see https://git.fairkom.net/emb/displ.eu/transposer 
